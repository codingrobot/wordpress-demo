#URL
https://wpdemo.codingrobot.com/


# Architecture
```
Google DNS ->
    Load balancer (w/ CDN) ->
         Instance group (auto scaler) ->
            Wordpress VM (CentOS + nginx + php-fpm) ->
                Managed MYSQL instance`
```
# Setup
1. Provision local terraform using `provision_terraform.sh`
2. Prepare immutable VM image with nginx+php7+wordpress which can be used by the auto scaler
    ```
    gcloud compute instance-templates create wordpress-centos-tpl \
        --project wpdemo-102018 \
        --source-instance=instance-1 \
        --source-instance-zone=us-west1-b
    ```
    N.B.: The option for creating VM images was not available on the UI as described in the
    [GCP docs](https://cloud.google.com/compute/docs/instance-templates/create-instance-templates#based-on-existing-instance)

    N.B.2: This command DID NOT preserve the content of the file system. Did not investigate any further due to time constraints.
    The VM instance created by the instance group was provisioned manually using `provision_wordpress.sh`.

3. Run `terraform apply` to provision GCP project + infrastructure components

# Manual steps

- Setup Frontend for the Global Load Balancer incl. managed SSL certificate (not available via Terraform)
- Enable Private IP for the managed mysql database (not available via Terraform)
- Setup A/AAAA DNS records (unable to get the anycast IP addresses of the LB frontend via Terraform)

# Issues
The following configuration steps are missing due to intricacies of the Google Cloud Platform.
These are standard features on AWS.

- Managed relational database is by default exposed to the public Internet. Private IP is beta feature and not supported by automation tool.
- Configuring "frontend" for the loadbalancer with managed SSL cert. cannot be automated.
- HTTP-to-HTTPS redirect is not a supported feature on the LB. https://groups.google.com/forum/#!topic/gce-discussion/r8JbwLp1Ux8 This is a basic feature available to the AWS LB
- Creating VM Image out of an existing VM is not posible via the Web Console. The described button in the documentation is missing. Using the `gcloud` requires thorough research
- Root GCP account did not have visibility over projects created by service accounts. Root GCP account has to acquire additional View permissions.
- Retrieving the Global IP (v4/v6) of a managed LB and creating the appropriate A/AAAA DNS records is not trivial to automate. Configuring via the Web Console involves error-prone copy pasting.




# Links
 - https://cloud.google.com/community/tutorials/managing-gcp-projects-with-terraform
 - https://wiki.centos.org/HowTos/php7
 - https://gist.github.com/darkn3rd/1f74fc3c426f4ca8890746e71a49a536