#!/usr/bin/env bash

# RUN THIS SCRIPT AS root

# TODO: set selinux+nginx
setenforce permissive

yum update -y
yum -y install centos-release-scl.noarch
yum install -y nginx rh-php71-php-fpm rh-php71-php-mysqlnd

systemctl enable rh-php71-php-fpm
systemctl enable nginx

curl -O https://wordpress.org/latest.tar.gz
tar xf latest.tar.gz -C /srv/
chown -R apache:apache /srv/wordpress/

cat << 'EOF' > /etc/nginx/nginx.conf
user apache;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/nginx/README.dynamic.
include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';
    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    include /etc/nginx/conf.d/*.conf;

    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /srv/wordpress/;

        location / {
                index index.php;
                try_files $uri $uri/ /index.php$is_args$args;
        }

        location ~ \.php$ {
            include        fastcgi_params;
            fastcgi_pass   127.0.0.1:9000;
            fastcgi_index  index.php;
            fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        }
    }
}
EOF


## TODO remove hard-coded db username/password

cat << 'EOF' > /srv/wordpress/wp-config.php
<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress-db');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', 'wordpress');

/** MySQL hostname */
define('DB_HOST', '10.105.0.3');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1u_@B^+O47*P3nn}a%D~3KT!;W S]mXx|igd^EgXi<;{uw^[f%?9taBw~(wa2_Vn');
define('SECURE_AUTH_KEY',  'wpP]8Lh2/7^yU^>V:{z97G[*d[s`^Km*sX g20D+&6=ooE{?}@FYL1yzyp%]kz6[');
define('LOGGED_IN_KEY',    ' 9u3[& @!4%)],ubi03Q17`ROe+`<#rJw*bigUyNLL/Hmy(a`/Ks:oquUZ>nYU1)');
define('NONCE_KEY',        ')BNayP`F5{xmpUv|W2%q4:WCm@xnVJYdBnyb]2D@I*%}]}u=m3Q8e6%c@fLF9xz&');
define('AUTH_SALT',        ',34MVHC%9GW1&Op)0zzJ@UGFp;_hCI{)yn%;>to}?+QW`0A1`R/1#a[TD^D$!b7R');
define('SECURE_AUTH_SALT', 't]5CA#L4T.@mov 3HAu*$$L[@gkedpUTdnB6Kmw2VQ<xL1ET 1YNCER?}4c4+ze,');
define('LOGGED_IN_SALT',   'uDmXvQ%as:H|e .EmMyP$MDBD5wm]`.&<Fx%QsV)fXcU(nJNtah},U.We {P+4*@');
define('NONCE_SALT',       'W0ODXIWiuSA6$leYySk4e4,/ryjL/+x!p7r$?Gk/-r8z+L#wRFpV.3Yk70|2`LcU');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
        define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

EOF

chown -R apache:apache /srv/wordpress/wp-config.php
