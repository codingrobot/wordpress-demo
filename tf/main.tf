variable "gcp_project_name" {}
variable "gcp_project_id" {}
variable "gcp_org_id" {}
variable "gcp_region" {}
variable "gcp_zone" {}
variable "gcp_billing_acct" {}
variable "gcp_dns_name" {}
variable "gcp_db_user_username" {}
variable "gcp_db_user_password" {}

provider "google" {
  region = "${var.gcp_region}"
  zone = "${var.gcp_zone}"
}

//resource "google_project" "project" {
//  name = "${var.gcp_project_name}"
//  project_id = "${var.gcp_project_id}"
//  org_id = "${var.gcp_org_id}"
//  billing_account = "${var.gcp_billing_acct}"
//}

resource "google_project_services" "project" {
  project = "${var.gcp_project_id}"

  services = [
    "compute.googleapis.com",
    "oslogin.googleapis.com",
    "dns.googleapis.com",
    "servicenetworking.googleapis.com"
  ]
}

module "database" {
  source = "./modules/database"

  gcp_project_id = "${var.gcp_project_id}"
  gcp_region = "${var.gcp_region}"
  gcp_db_user_username = "${var.gcp_db_user_username}"
  gcp_db_user_password = "${var.gcp_db_user_password}"
}

module "app_server" {
  source = "./modules/app_server"

  gcp_project_id = "${var.gcp_project_id}"
  gcp_zone = "${var.gcp_zone}"
  gcp_dns_name = "${var.gcp_dns_name}"

  gcp_db_ip_address = "${module.database.gcp_db_ip}"
}
