variable "gcp_project_id" {}
variable "gcp_zone" {}
variable "gcp_dns_name" {}
variable "gcp_db_ip_address" {}

resource "google_compute_health_check" "wordpress_healthcheck" {
  name = "wordpress-healthcheck"
  project = "${var.gcp_project_id}"
  check_interval_sec = 60
  timeout_sec = 1
  healthy_threshold = 2
  unhealthy_threshold = 10

  http_health_check {
    request_path = "/"
    port = "80"
  }
}

resource "google_compute_instance_group_manager" "wordpress_instancegrp_mngr" {
  name = "wordpress-webserver"
  project = "${var.gcp_project_id}"
  base_instance_name = "wordpress-app"
  instance_template = "projects/wpdemo-102018/global/instanceTemplates/wordpress-centos-tpl" # TODO automate/extract this
  update_strategy = "REPLACE"

  named_port {
    name = "http"
    port = 80
  }
}

resource "google_compute_autoscaler" "wordpres_webserver_scaler" {
  name = "wordpress-webserver-scaler"
  zone = "${var.gcp_zone}"
  target = "${google_compute_instance_group_manager.wordpress_instancegrp_mngr.self_link}"
  project = "${var.gcp_project_id}"

  autoscaling_policy = {
    max_replicas = 3
    min_replicas = 1
    cooldown_period = 60

    cpu_utilization {
      target = 0.8
    }
  }
}

resource "google_compute_backend_service" "wordpress_backend" {
  name = "wordpress-backend"
  port_name = "http"
  protocol = "HTTP"
  timeout_sec = 10
  enable_cdn = true
  project = "${var.gcp_project_id}"
  backend {
    group = "${google_compute_instance_group_manager.wordpress_instancegrp_mngr.instance_group}"
  }

  health_checks = [
    "${google_compute_health_check.wordpress_healthcheck.self_link}"]
}


resource "google_compute_global_forwarding_rule" "default_v4" {
  name = "default-rule-ipv4"
  project = "${var.gcp_project_id}"
  target = "${google_compute_target_http_proxy.default.self_link}"
  port_range = "80"
  # TODO switch to 443
}

resource "google_compute_target_http_proxy" "default" {
  name = "test-proxy"
  project = "${var.gcp_project_id}"
  url_map = "${google_compute_url_map.default.self_link}"
}

resource "google_compute_url_map" "default" {
  name = "url-map"
  project = "${var.gcp_project_id}"
  default_service = "${google_compute_backend_service.wordpress_backend.self_link}"

  host_rule {
    hosts = [
      "${var.gcp_dns_name}"]
    path_matcher = "allpaths"
  }

  path_matcher {
    name = "allpaths"
    default_service = "${google_compute_backend_service.wordpress_backend.self_link}"
  }
}

resource "google_dns_managed_zone" "wpdemo_zone" {
  name = "wpdemo-zone"
  dns_name = "${var.gcp_dns_name}"
}
