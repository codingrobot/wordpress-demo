variable "gcp_dns_name" {}
variable "gcp_dns_ipv4" {}
variable "gcp_dns_ipv6" {}

resource "google_dns_managed_zone" "wpdemo_zone" {
  name = "wpdemo-zone"
  dns_name = "${var.gcp_dns_name}"
}


resource "google_dns_record_set" "wordpress_a_record" {
  name = "${var.gcp_dns_name}"
  type = "A"
  ttl = 300

  managed_zone = "${var.gcp_dns_name}"

  rrdatas = ["${var.gcp_dns_ipv4}"]
}

resource "google_dns_record_set" "wordpress_aaaa_record" {
  name = "${var.gcp_dns_name}"
  type = "AAAA"
  ttl = 300

  managed_zone = "${var.gcp_dns_name}"

  rrdatas = ["${var.gcp_dns_ipv6}"]
}