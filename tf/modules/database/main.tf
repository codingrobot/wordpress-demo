variable "gcp_project_id" {}
variable "gcp_region" {}
variable "gcp_db_user_username" {}
variable "gcp_db_user_password" {}
variable "gcp_db_instance_name" { default = "wp-mysql" }
variable "gcp_db_instance_type" { default = "db-f1-micro" }

resource "google_sql_database_instance" "db" {
  name = "${var.gcp_db_instance_name}"
  project = "${var.gcp_project_id}"
  database_version = "MYSQL_5_7"
  region = "${var.gcp_region}"


  settings {
    tier = "${var.gcp_db_instance_type}"
  }
}

resource "google_sql_database" "wordpress" {
  name = "wordpress-db"
  project = "${var.gcp_project_id}"
  instance = "${google_sql_database_instance.db.name}"
}

resource "google_sql_user" "users" {
  project = "${var.gcp_project_id}"
  instance = "${google_sql_database_instance.db.name}"
  name = "${var.gcp_db_user_username}"
  password = "${var.gcp_db_user_password}"
  host = "%"
}

output "gcp_db_ip" {
  value = "${google_sql_database_instance.db.first_ip_address}"
}