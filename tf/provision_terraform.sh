#!/usr/bin/env bash
export TF_ADMIN=${USER}-terraform-admin
export TF_CREDS=~/.config/gcloud/${USER}-terraform-admin.json

export GOOGLE_APPLICATION_CREDENTIALS=${TF_CREDS}
export GOOGLE_PROJECT=${TF_ADMIN}

gcloud projects create ${TF_ADMIN} \
  --organization ${TF_VAR_ORG_ID} \
  --set-as-default

# Link Project to Billing Account
gcloud beta billing projects link ${TF_ADMIN} \
--billing-account ${TF_VAR_BILLING_ACCOUNT}


## Source: https://gist.github.com/darkn3rd/1f74fc3c426f4ca8890746e71a49a536

# create service account in Terraform Admin Project
gcloud iam service-accounts create terraform \
  --display-name "Terraform Admin Account"

# download JSON credentials
gcloud iam service-accounts keys create ${TF_CREDS} \
  --iam-account terraform@${TF_ADMIN}.iam.gserviceaccount.com

# grant service account permission to view Admin Project & Manage Cloud Storage
for ROLE in 'viewer' 'storage.admin'; do
  gcloud projects add-iam-policy-binding ${TF_ADMIN} \
    --member serviceAccount:terraform@${TF_ADMIN}.iam.gserviceaccount.com \
    --role roles/${ROLE}
done

# Enable API for terraform
for API in 'cloudresourcemanager' 'cloudbilling' 'iam' 'compute'; do
  gcloud services enable "${API}.googleapis.com"
done

# Grant service account permissions to create projects & assign billing accounts
for ROLE in 'resourcemanager.projectCreator' 'billing.user'; do
  gcloud organizations add-iam-policy-binding ${TF_VAR_ORG_ID} \
    --member serviceAccount:terraform@${TF_ADMIN}.iam.gserviceaccount.com \
    --role roles/${ROLE}
done